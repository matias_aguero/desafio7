import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalculadoraComponent } from './components/routes/calculadora/calculadora.component';
import { ContactoComponent } from './components/routes/contacto/contacto.component';
import { ErrorComponent } from './components/routes/error/error.component';
import { GaleryComponent } from './components/routes/galery/galery.component';
import { HeroComponent } from './components/routes/hero/hero.component';

const routes: Routes = [
  {
    path:'inicio',
    component: HeroComponent
  },
  {
    path:'gelery',
    component: GaleryComponent
  },
  {
    path:'calculadora',
    component: CalculadoraComponent
  },
  {
    path:'contacto',
    component: ContactoComponent
  },
  {
    path:'**',
    component: ErrorComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
