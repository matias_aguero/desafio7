import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroComponent } from './hero/hero.component';
import { GaleryComponent } from './galery/galery.component';
import { CalculadoraComponent } from './calculadora/calculadora.component';
import { ContactoComponent } from './contacto/contacto.component';
import { ErrorComponent } from './error/error.component';



@NgModule({
  declarations: [
    HeroComponent,
    GaleryComponent,
    CalculadoraComponent,
    ContactoComponent,
    ErrorComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
